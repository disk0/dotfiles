#!/usr/bin/env bash

set -e

pinstall brew ruby
pprint info-ok "Ruby is ready to shine"
