export ATOM_REPOS_HOME="${PROJECTS}/atom-dev"

# Ignores shellcheck no-shebang errors
export SHELLCHECK_OPTS="-e SC2148"
